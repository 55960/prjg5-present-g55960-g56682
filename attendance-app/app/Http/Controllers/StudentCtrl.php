<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Models\Students;

class StudentCtrl extends Controller{

    public function getStudents(){
        $students = Students::getStudents();
        return view('ListStudent', ['students' => $students]);
    }

    public function getHoraire(){
        $rep = Students::getHoraire();
        return view('horaireView', ['horaire' => $rep]);
    }

    public function delStudent(Request $req){
        Students::deleteStudent($req->item_id);
        return redirect('/');
    }

    public function ajoutStudent(Request $req){
        $req->validate([
            'matricule' => 'required',
            'nom' => 'required',
            'prenom' => 'required',
        ]);
        Students::ajoutStudent($req->matricule,$req->nom,$req->prenom);
        return redirect('/');
    }

    public function horaireApi(){
        $rep = Students::getHoraire();
        echo json_encode($rep,JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES);
    }

    public function horaireCour($courId){
        $rep = Students::cour($courId);
        echo json_encode($rep, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES);
    }
}