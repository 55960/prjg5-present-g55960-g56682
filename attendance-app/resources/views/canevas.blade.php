<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="shortcut icon" href="{{ secure_asset('favicon.ico') }}">
    <link rel="stylesheet" href="{{secure_asset('css/app.css')}}">
    <title>@yield('title')</title>
</head>
<body>
    <header><h1>@yield('title_header')</h1></header>
    <nav>
        <a href="{{Route('students')}}">List of student</a>
        <a href="{{ route('addStudent') }}">Ajouter un etudiant</a>
        <a href="/horaire">horaire</a>
    </nav>
    <main>@yield('content')</main>
    <footer>g55960 - g56682 - SRV</footer>
</body>
</html>
