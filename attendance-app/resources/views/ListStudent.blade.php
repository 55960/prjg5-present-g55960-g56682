@extends('canevas')
@section('title', 'student')
@section('title_header', 'Liste des présences')
@section('content')

<table>
    <th>
        Matricule
    </th>
    <th>
        Nom
    </th>
    <th>
        Prénom
    </th>
    <th>
    </th>
    @foreach($students as $item)
    <tr>
        <td class="matricule">
            {{$item->matricule}}
        </td>
        <td>
            {{$item->nom}}
        </td>
        <td class="prenom">
            {{$item->prenom}}
        </td>
        <td>
            <form action="/" method="post">
                {{csrf_field()}}
                <input type="hidden" name="item_id" value="{{$item->matricule}}">
                <input type="submit" name="delete_student" value="Delete" class="delete-button">
            </form>     
        </td>
    </tr>
    @endforeach
</table>
@endsection